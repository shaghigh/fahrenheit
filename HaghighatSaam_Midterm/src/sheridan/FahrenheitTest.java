package sheridan;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import sheridan.Fahrenheit;

public class FahrenheitTest {
	
	private String temperatureToTest;
	
	public FahrenheitTest(String val) {
		temperatureToTest = val;
	}
	
	//regular
	@Test
	public void testConvertFromCelsius(){
		int celsius = Fahrenheit.convertFromCelsius(0);
		System.out.println(this.temperatureToTest);
		assertTrue("The temperature was not calculated properly", celsius == 0);	
	}
	
	//exceptional
	@Test
	public void testConvertFromCelsiusException(){
		int celsius = Fahrenheit.convertFromCelsius((int) 4.5);
		System.out.println(this.temperatureToTest);
		assertTrue("The temperature was not calculated properly", celsius == 5);	
	}
	
	//boundaries
	@Test
	public void testConvertFromCelsiusInBound(){
		int celsius = Fahrenheit.convertFromCelsius((int) 4.1);
		System.out.println(this.temperatureToTest);
		assertTrue("The temperature was not calculated properly", celsius == 4);	
	}
	
	@Test
	public void testConvertFromCelsiusOutBound(){
		int celsius = Fahrenheit.convertFromCelsius((int) 4.9);
		System.out.println(this.temperatureToTest);
		assertTrue("The temperature was not calculated properly", celsius == 5);	
	}
}
